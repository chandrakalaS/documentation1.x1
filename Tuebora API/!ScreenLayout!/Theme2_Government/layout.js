/* Custom setting names. Used to hook into RH settings mechanism */
var lastVisitedTopic = "cust_last_visited_topic";
var lastSideBarWidth = "cust_last_sidebar_width";

var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
if(iOS && gbBlockIOSScaling) {/* Only when scaling is disabled */
	window.onorientationchange = function()
	{
		setTimeout("window.location.reload()", 500);
	}
}
var ua = navigator.userAgent;
if( ua.indexOf("Android") >= 0 )
{
  
  var androidversion = parseFloat(ua.slice(ua.indexOf("Android")+8)); 
  if (androidversion <= 2.3)
  {
	var tmpMetaTagsList = document.getElementsByTagName('meta');
	var contentString = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0";
	for (i=0; i<tmpMetaTagsList.length; i++)
	{
		if (tmpMetaTagsList[i].name == "viewport")
			tmpMetaTagsList[i].content = contentString;
	}
	if(i == tmpMetaTagsList.length)
	{
		var metaTag = document.createElement('meta');
		metaTag.setAttribute("name", "viewport");
		metaTag.setAttribute("content", contentString);
		var headTags = document.getElementsByTagName('head');
		headTags[0].appendChild(metaTag);
	}
  }
}

$(function(){/* Run on DOM ready */

	/* Use RH's event scheduling service. This service makes custom script work better with RH widgets. Adding scripts on DOM ready may interfere with widget loading. */
	addRhLoadCompleteEvent(initializeLayout);
	
});

function initializeLayout() {/* This function makes changes to the layout, including widgets. Called through RH event handler. */
		
	readSetting(lastVisitedTopic, changeTopicLink, false, false);/* Check for the latest visited topic. */
	
	setCheckBoxes();/* Change checkboxes into On/Off button */
	
	setLocalisation();
	
}
function setLocalisation() {
	var TOCString = $("#localisation_toc").text();
	var IDXString = $("#localisation_idx").text();
	var GLOString = $("#localisation_glo").text();
	var FTSString = $("#localisation_fts").text();
	var BACKString = $("#localisation_back").text();
	
	/* Set TOC title */
	$tocBar = $("div.bar_toc");
	$tocBar.attr("title",TOCString);
	$tocBar.children().attr("title",TOCString);
	/* Set IDX title */
	$idxBar = $("div.bar_index");
	$idxBar.attr("title",IDXString);
	$idxBar.children().attr("title",IDXString);
	/* Set GLO title */
	$gloBar = $("div.bar_glossary")
	$gloBar.attr("title",GLOString);
	$gloBar.children().attr("title",GLOString);
	/* Set FTS title */
	$ftsBar = $("div.bar_search")
	$ftsBar.attr("title",FTSString);
	$ftsBar.children().attr("title",FTSString);
	/* Set BACK title */
	$topicBar = $("div.bar_topic");
	$topicBar.attr("title",BACKString);
	$topicBar.children().attr("title",BACKString);
}
/* Remember last visited topic */
function setTopic() {
	saveSetting(lastVisitedTopic, document.location.toString(), false);/* Save the URL of the topic. */
}
function changeTopicLink(url, arg1, arg2) {/* Amend return link. Change default topic to last visited topic. */
	if(url != "") {
		$("a.back_to_last_topic").attr("href", url);
	}
}
//Turn checkboxes to on/off switches on Run
function setCheckBoxes() {
	/* 
		Credit for On/Off switch logic: http://blog.dbrgn.ch/2011/11/21/jquery-checkbox-on-off-toggleswitch/
		
		Method is shared under Creative Commons license Attribution-ShareAlike 3.0 Unported - http://creativecommons.org/licenses/by-sa/3.0/
		
	*/
	$('.switch').each(function() {
		var checkbox = $(this).children('input[type=checkbox]');
		var toggle = $(this).children('label.switch-toggle');
		if (checkbox.length) {
			
			checkbox.addClass('hidden');
			toggle.removeClass('hidden');
			
			if (checkbox[0].checked) {
				toggle.addClass('on');
				toggle.removeClass('off');
				toggle.text(toggle.attr('data-on'));
			} else { 
				toggle.addClass('off');
				toggle.removeClass('on');
				toggle.text(toggle.attr('data-off'));
			};
		}
	});
	$('.switch-toggle').click(function(){
		
		var checkbox = $(this).siblings('input[type=checkbox]')[0];
		var toggle = $(this); 
		
		if (checkbox.checked) {
			toggle.addClass('off');
			toggle.removeClass('on');
			toggle.text(toggle.attr('data-off'));
		} else {
			toggle.addClass('on');
			toggle.removeClass('off');
			toggle.text(toggle.attr('data-on'));
		}
	});
}