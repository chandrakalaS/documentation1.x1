	chrome.app.runtime.onLaunched.addListener(function() {
  var windowWidth = 300;
  var windowHeight = 325;
  chrome.app.window.create('popup.html', {
    outerBounds: { // 'bounds' is deprecated, and you want full window size
      width: windowWidth,
      height: windowHeight,
      left: screen.availWidth - windowWidth,
      top: screen.availHeight - windowHeight,
    },
    resizable: false,
    frame: 'none'
  });      
});