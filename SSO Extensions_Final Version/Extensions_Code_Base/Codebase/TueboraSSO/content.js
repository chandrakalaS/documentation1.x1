

var SelfUrl = '';
var browserType = '';

$(function(){
	if(navigator.userAgent.search("Chrome") >= 0){
		browserType = "chrome";
		chrome.storage.sync.get("url", function (obj) {
			SelfUrl = obj.url;
		});
	} else {
		browserType = "firefox";
		var gettingItem = browser.storage.local.get('url');
		
		  gettingItem.then((res) => {
				SelfUrl = res.url;
			});

	}
	
	
		
setTimeout(function(){
$("body").addClass('sso-extension-installed');

	checkUserSession();
	$("body").off('click','a.appLink');
	$("body").on('click','a.appLink',function(e){
		e.stopPropagation();
		checkMfaforAsset($(this).attr('href'), $(this).attr('data-app-id'))
		return false;
		//processAppURL($(this).attr('href'));	
	});

	var activeTab = window.location.host;
	 validateURLandLogin(activeTab);
//attemptLogin
//checkMFAEnabled
	$("#sid").on('click',attemptLogin);
		$("body").on('click','#logOutSession',function(){	
			LogoutUser();
			$('#newapp style').remove();
		});
	},50);
}); 

function checkMfaforAsset(assetUrl, assetId){
	$.ajax({
	   type:"POST",
	   url:SelfUrl+"singlesignon/sso/checkMfaEnabledforAsset",
	   data:{'assetId':assetId},  
	   async:false,
	   success: function(msg){
		   var mfaSettings  = $.parseJSON(msg);
			if(mfaSettings == 'portal') {                	
				processAppURL(assetUrl);	
			} else {
				$("#err-msg").html("You are not allowed to perform this action. Please login to portal.");
				$("#err-msg").show(500);
				setTimeout(function(){
					$("#err-msg").hide(500);
				},5000);
			}
	   }
	});
	return false;
}

function checkUserSession(){
	var reqResponse = '';
	var req = new XMLHttpRequest();
	req.open("GET", SelfUrl+"singlesignon/sso/checkUserLoggedIn", false);
	req.onreadystatechange = function() {
	  if (req.readyState == 4) {
		if (req.status == 200) {
			if(req.responseText == 1){
				getActiveAppslists();
			}
			//document.write(req.responseText);
		}
	  }
	};
  req.send();
}
function LogoutUser(){
	
	var reqResponse = '';
	var req = new XMLHttpRequest();
	req.open("GET", SelfUrl+"Login/Logout", false);
	req.onreadystatechange = function() {
	  if (req.readyState == 4) {
		if (req.status == 200) {
			//alert(req.responseText);
				  document.getElementById('newapp').style.display = 'none';
				  document.getElementById('login_ext').style.display = 'block';
			//document.write(req.responseText);
		}
	  }
	};
  req.send();
}
function validateURLandLogin(activeTab){
 	var IsloginApp = getParameterByName('login');
	var assetId = getParameterByName('assetRef');
		var resp = '';
		if(IsloginApp == 'extn'){
			
			var reqResponse = '';
			var req = new XMLHttpRequest();
		  	req.open("GET", SelfUrl+"singlesignon/sso/checkAppexists?assetRef="+assetId, false);
		  	req.onreadystatechange = function() {
			  if (req.readyState == 4) {
				if (req.status == 200) {
					if(req.responseText != 0){
						resp = $.parseJSON(req.responseText);
					} else {
					}
				}
			  }
			};
		  req.send();
		  
		  	var credentials = resp.credentials;
			var inputattr = resp.attr;
			var btnId = '';
			var btnName = '';
			var InputType = 'text';
			var InputVal = '';
			var InputId = '';
			var frameId = '';
		  	$.each($.parseJSON(inputattr), function(k,v){
					var credInfo = $.parseJSON(credentials);
					var credInfovar = "credInfo."+v.propertyName;
					var credInfoVal = eval(credInfovar);
					var InputFieldHtml = v;
					if(v.propertyName == 'submitButton'){
						InputType = v.type;
						InputVal = v.value;
					} else {
						InputType = "text";						
					}
					if(v.frameId !== undefined)
						frameId = v.frameId;
						
					InputId = 	v.formEleId;
					var InputName = 	v.formEleName;
					if(InputType !="" && InputType !==undefined && InputType != 'input' && InputType!='button' && credInfoVal!="" && InputId!==undefined && InputId!=''){
						if(frameId != ''){
							document.getElementById(frameId).contentWindow.document.getElementById(InputId).value = credInfoVal;
						} else {
							document.getElementById(InputId).value = credInfoVal;							
						}
						
					} else if(btnName!==undefined) {
						
						//If there is no Id for the input field
						if(InputId !== undefined && InputId != ""){

							btnId = InputId;
						} else {
							if(credInfoVal!='' && credInfoVal!==undefined && InputName!== undefined){
								if(frameId != ''){
									var inputsObj = document.getElementById(frameId).contentWindow.document.getElementsByName(InputName);
								} else {
									var inputsObj = document.getElementsByName(InputName);
								}
								
								$.each(inputsObj, function(k, inp){
									inp.value = credInfoVal;
								});
							}
							
							btnName = InputName;
						}
					} else {
						$("button").click();
					}
			});
					setTimeout(function(){
						if(InputType == "input"){
							if(InputId !== undefined){
								if(frameId != ''){
									document.getElementById(frameId).contentWindow.document.getElementById(InputId).click();
								} else {
									document.getElementById(InputId).click();
								}
							} else {
								if(frameId != ''){
									var inputObj = document.getElementById(frameId).contentWindow.document.querySelectorAll('input[value="'+InputVal+'"]')[0];
									inputObj.disabled = false;
									inputObj.click();
								} else {
									var inputObj = document.querySelectorAll('input[value="'+InputVal+'"]')[0];
									inputObj.disabled = false;
									inputObj.click();
								}
							}
						} else {
							if(frameId != ''){
									var btnObj = document.getElementById(frameId).contentWindow.document.getElementsByTagName('button');
								} else {
									var btnObj = document.getElementsByTagName('button');
								}
							$.each(btnObj, function(k,v){
								cleanText = v.innerHTML.replace(/<\/?[^>]+(>|$)/g, "");
								if($.trim(v.innerHTML.toLowerCase()) == InputVal.toLowerCase() || cleanText.indexOf(InputVal) != -1){
									v.click();
									return false;
								}
							});
						}
						//document.getElementById(btnId).click();
					},10);
					setTimeout(function(){
						if(InputType == "input"){
							if(InputId !== undefined){
								if(frameId != ''){
									document.getElementById(frameId).contentWindow.document.getElementById(InputId).click();
								} else {
									document.getElementById(InputId).click();
								}
							} else {
								if(frameId != ''){
									var inputObj = document.getElementById(frameId).contentWindow.document.querySelectorAll('input[value="'+InputVal+'"]')[0];
									inputObj.disabled = false;
									inputObj.click();
								} else {
									var inputObj = document.querySelectorAll('input[value="'+InputVal+'"]')[0];
									inputObj.disabled = false;
									inputObj.click();
								}
							}
						} else {
							if(frameId != ''){
									var btnObj = document.getElementById(frameId).contentWindow.document.getElementsByTagName('button');
								} else {
									var btnObj = document.getElementsByTagName('button');
								}
							$.each(btnObj, function(k,v){
								cleanText = v.innerHTML.replace(/<\/?[^>]+(>|$)/g, "");
								if($.trim(v.innerHTML.toLowerCase()) == InputVal.toLowerCase() || cleanText.indexOf(InputVal) != -1){
									v.click();
									return false;
								}
							});
						}
					},1500);
		}
}
function processAppURL(apiUrl){
	if(browserType == 'chrome'){
		chrome.tabs.create({url: apiUrl});
	} else if(browserType == 'firefox'){
		browser.tabs.create({url:apiUrl});
		
		//browser.tabs.create({url: apiUrl,tab:true});
	}
	
	return false;


}
function checkMFAEnabled(){
	$.ajax({
	   type:"POST",
	   url:SelfUrl+"singlesignon/sso/checkMfaEnabled",  
	   async:false,
	   success: function(msg){
		   var mfaSettings  = $.parseJSON(msg);
			if(mfaSettings.length ==0 || mfaSettings==null || mfaSettings==undefined) {                  	
				//getActiveAppslists();
				attemptLogin();
			} else {
				
				$("#err-msg").html("You are not allowed to login from the extension. Please login from the Portal");
				$("#err-msg").show(500);
				setTimeout(function(){
					$("#err-msg").hide(500);
				},5000);
			}
	   }
	});
	return false;
}
function attemptLogin(){
	var uId = $("#username").val();
	var pId = $("#password").val();	
	var params = $("form[name='login']").serialize();
	var errMsg = '';
	if(uId == '' || uId==null){
		$("#err-msg").html('Username required to Login');
		$("#err-msg").show(500);
		setTimeout(function(){
			$("#err-msg").hide(500);
		},5000);
		return false;
	}
	if(pId == '' || pId==null){
		$("#err-msg").html('Password required to Login');
		$("#err-msg").show(500);
		setTimeout(function(){
			$("#err-msg").hide(500);
		},5000);
		return false;
	}

	 var params = $("form[name='login']").serialize();
	 $.ajax({
           type:"POST",
           url:SelfUrl+"Login/authenticate",  
           data:params,
           async:false,
           success: function(msg){
                if(msg == 'OK') {                  	
					getActiveAppslists();
                } else {
					
					$("#err-msg").html(msg);
					$("#err-msg").show(500);
					setTimeout(function(){
						$("#err-msg").hide(500);
					},5000);
				}
		   }
	 });
	return false;
}

function getActiveAppslists(){
	var req = new XMLHttpRequest();
  	req.open("GET", SelfUrl+"singlesignon/sso/getActiveAppList?custUrl="+SelfUrl, false);
	  req.onreadystatechange = function() {
		  if (req.readyState == 4) {
			if (req.status == 200) {
				$("#newapp").show();
				$("#newapp").html(req.responseText);
				$("#login_ext").hide();
				//$("#home_url").attr("href",SelfUrl)
			}
		  }
		};
	 req.send();
	 
	 
	 
	 $('[data-toggle="tooltip"]').tooltip({
		 container: 'body'
		 });
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}