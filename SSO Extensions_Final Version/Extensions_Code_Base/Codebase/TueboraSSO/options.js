function saveOptions(e) {
	if(navigator.userAgent.search("Chrome") >= 0){
		 chrome.storage.sync.set({
			url: document.querySelector("#url").value,
		  }, function() {
			// Update status to let user know options were saved.
			var status = document.getElementById('status');
			status.textContent = 'Options saved.';
			setTimeout(function() {
			  status.textContent = '';
			}, 750);
		  });
	} else {
	  browser.storage.local.set({
		url: document.querySelector("#url").value
	  });
	}
	  e.preventDefault();
}

function restoreOptions() {
  var gettingItem = browser.storage.local.get('url');
  gettingItem.then((res) => {
    document.querySelector("#url").value = res.url	 || '';
  });
  
   
  
  
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("#save").addEventListener("click", saveOptions);
